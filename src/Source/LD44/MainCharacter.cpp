// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "MainGameMode.h"

AMainCharacter::AMainCharacter()
{
	this->bUseControllerRotationYaw = false;

	this->Health = 100;
}

void AMainCharacter::Kill()
{
	this->Health = 0;

	AMainGameMode* const GameMode = Cast<AMainGameMode>(this->GetWorld()->GetAuthGameMode());
	if (GameMode != nullptr)
	{
		GameMode->ShowDeathScreen();
	}
}

void AMainCharacter::ModifyHealth(int32 DeltaHealth)
{
	this->Health += DeltaHealth;

	if (this->Health <= 0)
	{
		this->Kill();
	}
	else if (this->Health > 100)
	{
		this->Health = 100;
	}
}

float AMainCharacter::GetHealthPercent() const
{
	return this->Health / 100.0f;
}
