// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "MainCharacter.generated.h"

UCLASS()
class LD44_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

	AMainCharacter();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta=(AllowPrivateAccess="true"))
	int32 Health;

public:
	UFUNCTION(BlueprintCallable, Category = Health)
	void Kill();

	UFUNCTION(BlueprintCallable, Category = Health)
	void ModifyHealth(int32 DeltaHealth);

	UFUNCTION(BlueprintCallable, Category = Health)
	float GetHealthPercent() const;

	FORCEINLINE int32 GetHealth() const { return this->Health; }
};
