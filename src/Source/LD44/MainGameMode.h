// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "Question.h"

#include "MainGameMode.generated.h"

UCLASS()
class LD44_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

	AMainGameMode();

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Questions, meta = (AllowPrivateAccess = "true"))
	TArray<FName> FailDialogs;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Questions, meta = (AllowPrivateAccess = "true"))
	TArray<FQuestion> Questions;

	UPROPERTY()
	FQuestion NextQuestion;

	UPROPERTY()
	uint32 bSecretEndingEnabled : 1;

public:
	UFUNCTION(BlueprintImplementableEvent)
	void PlayDialog(FName Id);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ShowEndingScreen();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ShowCreditsScreen();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ShowDeathScreen();

	UFUNCTION(BlueprintCallable, Category = Questions)
	void PlayFailDialog();

	UFUNCTION(BlueprintCallable, Category = Questions)
	void EnableSecretEnding();

	UFUNCTION(BlueprintCallable, Category = Questions)
	void AddProgress();

	UFUNCTION(BlueprintCallable, Category = Questions)
	void FinishDialog();

	UFUNCTION(BlueprintCallable, Category = Questions)
	int32 GetQuestionsRemaining() const;

	FORCEINLINE TArray<FQuestion> const GetQuestions() const { return this->Questions; }
};
