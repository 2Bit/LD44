// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainPlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "UI/PlayerHUD.h"
#include "MainCharacter.h"
#include "MainGameMode.h"

AMainPlayerController::AMainPlayerController()
{
	this->InputYawScale = 1.0f;
	this->InputPitchScale = -1.0f;

	static ConstructorHelpers::FClassFinder<UPlayerHUD> PlayerHUDClass(TEXT("/Game/UI/BP_PlayerHUD.BP_PlayerHUD_C"));
	if (PlayerHUDClass.Succeeded())
	{
		this->PlayerHUDClass = PlayerHUDClass.Class;
	}
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	this->PlayerHUD = CreateWidget<UPlayerHUD>(this, this->PlayerHUDClass);
	this->PlayerHUD->AddToViewport();
	this->PlayerHUD->SetVisibility(ESlateVisibility::Hidden);

	this->PlayerCameraManager->ViewYawMax = 45.0f;
	this->PlayerCameraManager->ViewYawMin = -45.0f;
	this->PlayerCameraManager->ViewPitchMax = 45.0f;
	this->PlayerCameraManager->ViewPitchMin = -45.0f;

	this->SetInputMode(FInputModeGameAndUI());
	this->bShowMouseCursor = true;
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	this->InputComponent->BindAction(TEXT("Skip"), EInputEvent::IE_Pressed, this, &AMainPlayerController::Skip);

	this->InputComponent->BindAxis(TEXT("Turn"), this, &AMainPlayerController::Turn);
	this->InputComponent->BindAxis(TEXT("Look"), this, &AMainPlayerController::Look);
}

void AMainPlayerController::Skip()
{
	AMainGameMode* const GameMode = Cast<AMainGameMode>(this->GetWorld()->GetAuthGameMode());
	if (GameMode != nullptr)
	{
		GameMode->FinishDialog();
	}
}

void AMainPlayerController::Turn(float Value)
{
	this->AddYawInput(Value);
}

void AMainPlayerController::Look(float Value)
{
	this->AddPitchInput(Value);
}
