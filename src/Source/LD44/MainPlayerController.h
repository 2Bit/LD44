// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "MainPlayerController.generated.h"

UCLASS()
class LD44_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()

	AMainPlayerController();

protected:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI, meta=(AllowPrivateAccess="true"))
	TSubclassOf<class UPlayerHUD> PlayerHUDClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = UI, meta=(AllowPrivateAccess="true"))
	class UPlayerHUD* PlayerHUD;

public:
	void Skip();

	void Turn(float Value);
	void Look(float Value);

	FORCEINLINE class UPlayerHUD* const GetPlayerHUD() const { return this->PlayerHUD; }
};
