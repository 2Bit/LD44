// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Question.generated.h"

USTRUCT(BlueprintType)
struct FAnswer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Text;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Penalty;
};

USTRUCT(BlueprintType)
struct FQuestion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Intro;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Dialog;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Question;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnswer Answer1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnswer Answer2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnswer Answer3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnswer Answer4;
};
