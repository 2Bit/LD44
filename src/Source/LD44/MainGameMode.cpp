// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainGameMode.h"

#include "MainPlayerController.h"
#include "MainCharacter.h"
#include "UI/PlayerHUD.h"
#include "Kismet/GameplayStatics.h"

AMainGameMode::AMainGameMode()
{
	this->PlayerControllerClass = AMainPlayerController::StaticClass();
	this->DefaultPawnClass = AMainCharacter::StaticClass();
}

void AMainGameMode::PlayFailDialog()
{
	if (this->FailDialogs.Num() > 0)
	{
		int32 const FailDialogIndex = FMath::RandRange(0, this->FailDialogs.Num() - 1);

		this->PlayDialog(this->FailDialogs[FailDialogIndex]);

		this->FailDialogs.RemoveAt(FailDialogIndex);
	}
}

void AMainGameMode::EnableSecretEnding()
{
	this->bSecretEndingEnabled = true;
}

void AMainGameMode::AddProgress()
{
	AMainPlayerController* PlayerController = Cast<AMainPlayerController>(this->GetWorld()->GetFirstPlayerController());
	if (PlayerController != nullptr && PlayerController->GetPlayerHUD() != nullptr)
	{
		PlayerController->GetPlayerHUD()->Configure(FQuestion());
	}

	if (this->Questions.Num() > 0)
	{
		this->NextQuestion = this->Questions[0];
		this->Questions.RemoveAt(0);

		if (this->NextQuestion.Intro == NAME_None)
		{
			this->FinishDialog();
		}
		else
		{
			this->PlayDialog(this->NextQuestion.Intro);
		}
	}
	else
	{
		if (this->bSecretEndingEnabled)
		{
			this->PlayDialog(TEXT("Ending2"));

			this->ShowCreditsScreen();
		}
		else
		{
			this->PlayDialog(TEXT("Ending1"));

			this->ShowEndingScreen();
		}
	}
}

void AMainGameMode::FinishDialog()
{
	if (!this->NextQuestion.Question.IsEmpty())
	{
		AMainPlayerController* PlayerController = Cast<AMainPlayerController>(this->GetWorld()->GetFirstPlayerController());
		if (PlayerController != nullptr && PlayerController->GetPlayerHUD() != nullptr)
		{
			PlayerController->GetPlayerHUD()->Configure(this->NextQuestion);
		}

		this->NextQuestion = FQuestion();
	}
}

int32 AMainGameMode::GetQuestionsRemaining() const
{
	return this->Questions.Num();
}
